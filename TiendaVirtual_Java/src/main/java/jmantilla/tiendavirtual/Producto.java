/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmantilla.tiendavirtual;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmantilla.conexion.Conexion;

/**
 *
 * @author Usuario
 */
public class Producto {
    
    private int idProductos;
    private String nombreProducto;
    private int stockMinimo;
    private int stockMaximo;
    private int precioProd;
    private String  unidadProd;
    private String categoriaProd;
    
    public Producto(String nombreProducto,int stockMinimo,int stockMaximo,String  unidadProd, String categoriaProd){
        super();
        try {
            Conexion conn = new Conexion();
            Connection c = conn.iniciarConexion();
            ResultSet resultado = conn.consultar(c, "SELECT * FROM productos WHERE nombreProducto='"+nombreProducto+"'");
            if (resultado == null){ // No existe el producto en la base de datos
                this.nombreProducto = nombreProducto;
                this.stockMinimo = stockMinimo;
                this.stockMaximo = stockMaximo;
                this.unidadProd = unidadProd;
                this.categoriaProd = categoriaProd;
            }else{
                System.out.println("No se puede crear porque ya existe");
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public int getIdProductos() {
        return this.idProductos;
    }

    public void setIdProductos(int idProductos) {
        this.idProductos = idProductos;
    }

    public String getNombreProducto() {
        return this.nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getStockMinimo() {
        return this.stockMinimo;
    }

    public void setStockMinimo(int stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public int getStockMaximo() {
        return stockMaximo;
    }

    public void setStockMaximo(int stockMaximo) {
        this.stockMaximo = stockMaximo;
    }

    public int getPrecioProd() {
        return precioProd;
    }

    public void setPrecioProd(int precioProd) {
        this.precioProd = precioProd;
    }

    public String getUnidadProd() {
        return unidadProd;
    }

    public void setUnidadProd(String unidadProd) {
        this.unidadProd = unidadProd;
    }

    public String getCategoriaProd() {
        return categoriaProd;
    }

    public void setCategoriaProd(String categoriaProd) {
        this.categoriaProd = categoriaProd;
    }

     // Guardar
    public void guardar() throws ClassNotFoundException, SQLException{
        //CRUD - C
        Conexion con = new Conexion(); //instancia
        Connection c = con.iniciarConexion();
        String  sql = "INSERT INTO productos(idProductos, nombreProducto, stockMinimo, stockMaximo, precioProd, unidadProd, categoriaProd) VALUES("+getIdProductos()+",'"+getNombreProducto()+"',"+getStockMinimo()+","+getStockMaximo()+","+getPrecioProd()+",'"+getUnidadProd()+"','"+getCategoriaProd()+"');";
        System.out.println("Sentencia guardar: "+sql);
        Statement statement = c.createStatement();
        statement.execute(sql);
        con.closeCon(c);
        /*
        numeros = 12321, 343332.
        INSERT INTO tabla1 (COL1, COL2) VALUES (32123, 433.32);
        NO NUMERICOS -> COMILLAS
        INSERT INTO tabla2(nombre, fecha_nacimiento) VALUES ('Emerson', '1900-01-01')
        
        */
    }
    
    public void consultar() throws ClassNotFoundException, SQLException{
       
        Conexion con = new Conexion();
        Connection c = con.iniciarConexion();
        
        con.closeCon(c);
        
    }

    
    // ACTUALIZAR
   public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        Conexion con = new Conexion();
        String sql = "UPDATE productos SET nombreProducto = "+getNombreProducto()+",stockMinimo = "+getStockMinimo()+", stockMaximo = "+getStockMaximo()+", precioProd = "+getPrecioProd()+", unidadProd = "+getUnidadProd()+", categoriaProd = "+getCategoriaProd()+" WHERE idProductos = getIdProductos"+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    
    
    }

    
    // ELIMINAR
   public void borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D
        Conexion con = new Conexion();
        String sql = "DELETE FROM productos WHERE idProductos ="+getIdProductos()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    
    }


    
    @Override
    
    
    public String toString() {
        return "Producto{" + "idProductos=" + idProductos + ", nombreProducto=" + nombreProducto + ", stockMinimo=" + stockMinimo + ", stockMaximo=" + stockMaximo + ", precioProd=" + precioProd + ", unidadProd=" + unidadProd + ", categoriaProd=" + categoriaProd + '}';
    }
        
        
    
    
    
}
