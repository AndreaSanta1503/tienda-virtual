/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmantilla.conexion;





/* aqui comienza la conexion de Tutota*/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Conexion {
    
    // Atributos
    
    public Connection con = null; // traido del reto 5
    public Statement stmt = null; // traido del reto 5
    //private ResultSet rs = null; // traido del reto 5
    String servidor="localhost";
    String basedatos="tienda_virtual";
    String url="jdbc:mysql://"+servidor+":3306/"+basedatos;
    String user="root"; // ususario de la base de datos
    String password= "1234";// contraseña de acceso a la base de datos

    // Constructor
    public Conexion(){
        // jdbc:mysql protocolo de conexión a la DB 
        // luego viene el dominio - localhost -> :3306 es el puerto    
    }
    
    // Métodos
    public Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection  connection=DriverManager.getConnection(this.url,this.user,this.password);
        System.out.println("Connection is Successful to the database "+url);
        boolean conexion_valida = connection.isValid(20000); // Pregunta o espera un tiempo (20 segundos) para comprobar que la conexion es valida
        if(conexion_valida = true){
            System.out.println("Conexion exitosa");
            return connection;
        }else{
            System.out.println("Conexion errada");
            return null;
        }     
    }
    
    public void closeCon(Connection c){
        try {
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public ResultSet consultar(Connection c, String q){
        try {
            ResultSet r = null;
            try {
                Statement s = c.createStatement();
                r = s.executeQuery(q);
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Error -> "+ex);
            }
            if(r.next() == true){
                return r;
            }else{
                System.out.println("La tabla/consulta no genera resultados/esta vacía");
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error -> "+ex);
        }
        return null;
    }
    
    
    public boolean insertarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }  
    
    public boolean borrarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }
// Mtodoque realiza una operacincomo UPDATE, DELETE, CREATE TABLE, entre otras
// y devuelve TRUE si la operacinfue existosa

    public boolean actualizarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }
    
    
    
    
    
}

